# Game of three - microservices

## design

The emphasis of this project has been set on using a fully automated serverless microservice approach.
This solution uses 2 independent microservices to accomplish the game of three;

`player1` and `player2` are two microservices that each start a game every 5 (8 for player 2) minutes, and also
provide a REST Api to start a new game and to get game results.
the two microservices communicate asynchronously, so even if player1 is down
when player2 starts a new game, the game is not lost.

A Dynamo DB table is used as a logfile service for both services.
This table is NOT meant as persistence for the services, in that case each microservice would
have got its own Database (rule of *dont-share-persistence* in microservices)

## configurability

the services get all their configuration via environment variables set up in the infrastructure deployment; so the same
code can run in dev, prod, etc

## infrastructure

all the infrastructure is written in code (terraform), no AWS UI has to be clicked for this to work - the whole
infrastructure is brought up by `npm run build && cd infrastructure && ./build.sh dev`

to run this in your own AWS account you just have to fork this project and provide AWS credentials (by setting env vars
AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in bitbucket's pipeline settings);

the api endpoints of the services are created on first infrastructure deployment and can be seen as terraform
outputs in the output of the pipeline (currently: 
playerservice1_url = https://4zvnotn8ri.execute-api.eu-west-1.amazonaws.com/prod/api and 
playerservice2_url = https://oeqwir8us4.execute-api.eu-west-1.amazonaws.com/prod/api)

## CI/CD and Environments
bitbucket provides integrated build pipelines (similar to CircleCI, TravisCI)

every push to master will start the pipeline defined in `bitbucket-pipeline.yaml` and results in both
microserves being deployed in AWS;

you can choose an environment name (currently hardcoded as `dev` in `bitbucket-pipelines.yml`) - every AWS resource
name is scoped to this environment by using the environment as name-suffixes;
by executing `sh build.sh prod` the exactly same infrastructure can be rolled out into a prod-environment

you can see the execution of the pipelines under https://bitbucket.org/ujann/gameofthree/addon/pipelines/home#!/

## AWS resources

no VM is used to set up this microservice environment; the application code is run by Lambda, GameMoves are persisted in
DynamoDB, communication between microservices is done via Kinesis (=AWS managed kafka)

## APIs

both players provide the following API:

- `GET {service-url}/api`: get the game move logs of the last ten minutes
- `GET {service-url}/api/{gameId}`: get the game move logs of a specific game
- `POST {service-url}/api`: let the player start a new game by posting a start value

examples: 

```
$: # throw a new game at player1:
$: curl --data '{"value": 25}' https://4zvnotn8ri.execute-api.eu-west-1.amazonaws.com/prod/api
{"message":"started Game","gameId":"992d209b-87ac-4c89-9909-a557a47b2e18","initalValue":56,"responseValue":19,"timestamp":"2018-08-11T04:47:54.642Z"}%

$: # now show the game logs:
$: curl  https://4zvnotn8ri.execute-api.eu-west-1.amazonaws.com/prod/api/992d209b-87ac-4c89-9909-a557a47b2e18
{
  "logEntries": [
    {
      "Initial": 56,
      "Timestamp": "2018-08-11T04:47:54.642Z",
      "Value": 19,
      "GameId": "992d209b-87ac-4c89-9909-a557a47b2e18",
      "Name": "player1-dev"
    },
    {
      "Initial": 19,
      "Timestamp": "2018-08-11T04:47:55.122Z",
      "Value": 6,
      "GameId": "992d209b-87ac-4c89-9909-a557a47b2e18",
      "Name": "player2-dev"
    },
    {
      "Initial": 6,
      "Timestamp": "2018-08-11T04:47:56.712Z",
      "Value": 2,
      "GameId": "992d209b-87ac-4c89-9909-a557a47b2e18",
      "Name": "player1-dev"
    },
    {
      "Initial": 2,
      "Timestamp": "2018-08-11T04:47:58.102Z",
      "Value": 1,
      "GameId": "992d209b-87ac-4c89-9909-a557a47b2e18",
      "Name": "player2-dev"
    }
  ]
}

$: # player 2 seems to have won ....
```

## Open Issues

- not much time has been invested into the domain model / "game logic"
- there is only 1 test yet
- error handling (invalid payloads, retries, ...) is minimal so far
- resource-dependencies have not been properly set yet in terraform, so from a fresh (phoenix) install you might get an AWS error at initial pipeline run (then just rerun the pipeline)
- performance could be improved by using kinesis batch reads (which would need a way to deduplicate messages)
