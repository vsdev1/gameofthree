process.env.LOG_TABLE_NAME = 'gameof3-log-table-dev';
process.env.PLAYER_NAME = 'player3';
process.env.OPPONENTS_STREAM_NAME = 'stream-player1-dev';
const apiGatewayEvent = {
  resource: '/{proxy+}',
  path: '/api',
  httpMethod: 'GET',
  headers: {},
  queryStringParameters: null,
  pathParameters: {
    proxy: 'api',
  },
  stageVariables: null,
  requestContext: {},
  body: null,
  isBase64Encoded: false,
};

const { handler } = require('./src/lambda');

handler(apiGatewayEvent)
  .then(
    res => console.log('Success', res),
    err => console.log('Error', err),
  );
