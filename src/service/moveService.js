// eslint-disable-next-line import/no-extraneous-dependencies
const AWS = require('aws-sdk');

const kinesis = new AWS.Kinesis({
  apiVersion: '2013-12-02',
  region: 'eu-west-1',
});

module.exports = {
  sendMoveToOpponent: async (move) => {
    const record = {
      Data: JSON.stringify({
        gameId: move.gameId,
        value: move.response,
      }),
      PartitionKey: `partition-${move.gameId}`,
    };
    return kinesis.putRecords({
      Records: [record],
      StreamName: process.env.OPPONENTS_STREAM_NAME,
    }).promise();
  },
};
