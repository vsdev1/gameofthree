/* eslint-disable import/no-extraneous-dependencies */
const AWS = require('aws-sdk');

const DynamoDBClient = new AWS.DynamoDB.DocumentClient({ region: 'eu-west-1' });

const DYNAMO_DB_TABLENAME = process.env.LOG_TABLE_NAME;

async function addLogEntry(gameMove) {
  const params = {
    TableName: DYNAMO_DB_TABLENAME,
    Item: {
      GameId: gameMove.gameId,
      Timestamp: gameMove.timestamp,
      Name: process.env.PLAYER_NAME,
      Value: gameMove.response,
      Initial: gameMove.initial,
    },
  };
  await DynamoDBClient.put(params).promise();
}

async function getLogEntriesForGameId(gameId) {
  const params = {
    TableName: DYNAMO_DB_TABLENAME,
    KeyConditionExpression: 'GameId = :gameId',
    ExpressionAttributeValues: {
      ':gameId': gameId,
    },
  };
  const result = await DynamoDBClient.query(params).promise();
  return result.Items.sort(entry => entry.Timestamp);
}

async function getLogEntries() {
  const tenMinutesAgo = new Date(Date.now() - 10 * 60 * 1000).toISOString();
  const params = {
    TableName: DYNAMO_DB_TABLENAME,
    FilterExpression: '#v > :tenMinutesAgo',
    ExpressionAttributeValues: { ':tenMinutesAgo': tenMinutesAgo },
    ExpressionAttributeNames: { '#v': 'Timestamp' },
  };
  const result = await DynamoDBClient.scan(params).promise();
  return result.Items.sort(entry => entry.Timestamp);
}

module.exports = {
  getLogEntriesForGameId,
  getLogEntries,
  addLogEntry,
};
