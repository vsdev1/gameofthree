const { sendMoveToOpponent } = require('../service/moveService');
const { addLogEntry } = require('../service/logService');
const Move = require('../model/move');

exports.handleStreamEvent = async ({ gameId, value }) => {
  console.log(`received a game-move: ${gameId} - ${value}`);
  if (value > 1) {
    const move = new Move(gameId);
    move.makeMove(value);
    console.log('reply with move:', move);
    await addLogEntry(move);
    await sendMoveToOpponent(move);
  } else {
    console.log(`Got Value ${value} - other player won gameId ${gameId}!`);
  }
};
