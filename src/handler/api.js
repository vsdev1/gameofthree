const ID_REGEX = /\/api\/(.+)/;

const uuid = require('uuid');
const { sendMoveToOpponent } = require('../service/moveService');
const { getLogEntries, getLogEntriesForGameId, addLogEntry } = require('../service/logService');
const Move = require('../model/move');

const response = (status, body) => ({
  statusCode: status,
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(body),
});

module.exports = {
  handleApiEvent: async (method, path, body) => {
    try {
      if (method === 'POST' && path === '/api') {
        const { value } = JSON.parse(body);
        const move = new Move(uuid());
        move.makeMove(value);
        await addLogEntry(move);
        await sendMoveToOpponent(move);
        console.log('start api move to opponent', move);
        return response(200, {
          message: 'started Game',
          ...move,
        });
      }
      if (method === 'GET' && ID_REGEX.test(path)) {
        const gameId = ID_REGEX.exec(path)[1];
        const logEntries = await getLogEntriesForGameId(gameId);
        return response(200, { logEntries });
      }
      if (method === 'GET' && path === '/api') {
        const logEntries = await getLogEntries();
        return response(200, { logEntries });
      }
      return response(404, 'Not found');
    } catch (error) {
      console.log(`Error  while serving API: ${error.message}`, error.stack);
      return response(500, error.message);
    }
  },
};
