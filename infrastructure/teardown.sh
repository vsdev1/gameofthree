#!/usr/bin/env bash
set -e

# check mount content
echo docker run -v $(pwd):/app/ --entrypoint "/bin/ls" -w /app/ hashicorp/terraform:light -al
docker run -v $(pwd):/app/ --entrypoint "/bin/ls" -w /app/ hashicorp/terraform:light -al

TF="docker run -v $(pwd):/app/ -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID -w /app/ hashicorp/terraform:light"

${TF} init -backend-config="bucket=gameof3" -backend-config="key=$1/gameof3/tf.state"
echo "destroying stage \"$1\""

${TF} destroy -var "stage=$1" -auto-approve
